REGISTRY = registry.cn-hangzhou.aliyuncs.com
REGISTRY_PASS =

SPACE = resium

start:
	docker-compose up -d

stop:
	docker-compose down

pull-images:
	docker pull $(REGISTRY)/$(SPACE)/resium-api:latest
	docker pull $(REGISTRY)/$(SPACE)/resium-web:latest
	docker pull $(REGISTRY)/$(SPACE)/resium-bot:latest
	docker pull $(REGISTRY)/$(SPACE)/resium-downhub:latest
	docker pull $(REGISTRY)/$(SPACE)/resium-acr-trigger:latest
	docker pull $(REGISTRY)/$(SPACE)/resium-cron:latest
	docker images | grep $(REGISTRY) | awk '{print "docker tag",$$1":"$$2,$$1":"$$2}' | sed -e 's#$(REGISTRY)/$(SPACE)/##2' | sh -x
	docker images | grep $(REGISTRY) | awk '{print "docker rmi",$$1":"$$2}' | sh -x

login-registry:
	docker login -u 1583096683@qq.com -p $(REGISTRY_PASS) $(REGISTRY)

run-acr-trigger:
	docker run -d --name resium-acr-trigger \
	-v /srv/resium/resium-acr-trigger/aliyun-cli:/root/.aliyun \
	-v /srv/resium/resium-acr-trigger/data:/data/resium-acr-trigger/data \
	resium-acr-trigger:latest

start-dev:
	docker-compose -f dev.yaml up -d

remove-dev:
	docker-compose -f dev.yaml down

remove-none-images:
	docker images | grep '<none>' | awk '{print $$3}' | xargs docker rmi || true
