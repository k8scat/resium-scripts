# resium-scripts

[GitHub](https://github.com/k8scat/resium-scripts) |
[Gitee](https://gitee.com/resium/resium-scripts)

## 发布说明

https://www.notion.so/k8scat/6b2e6c13fd4d4e71a4cb347d6c20db74

## Repositories

- [k8scat/resium-api](https://github.com/k8scat/resium-api)
- [k8scat/resium-web](https://github.com/k8scat/resium-web)
- [k8scat/resium-downhub](https://github.com/k8scat/resium-downhub)
- [k8scat/resium-app](https://github.com/k8scat/resium-app)
- [k8scat/resium-bot](https://github.com/k8scat/resium-bot)
- [k8scat/resium-acr-trigger](https://github.com/k8scat/resium-acr-trigger)

`resium-acr-trigger` 用于监控 `ACR` 镜像的构建状态，构建开始或完成时进行推送状态消息，目前使用的是钉钉。

## ACR 阿里云容器镜像服务

- [华东1（杭州）](https://cr.console.aliyun.com/cn-hangzhou/instance/repositories)
- 账号：1583096683@qq.com
- 命名空间：resium
