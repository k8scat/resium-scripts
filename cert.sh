#!/bin/bash
set -e

email=1583096683@qq.com
api_domain=api.resium.ncucoder.com
fs_domain=file.resium.ncucoder.com
web_domain=resium.cn

volume_dir="$(pwd -P)/archive"
if [[ -d "${volume_dir}" ]]; then
  rm -rf ${volume_dir}
fi

function new_cert() {
  domain=$1
  docker run --rm \
    -p 80:80 \
    -v "${volume_dir}:/etc/letsencrypt/archive" \
    certbot/certbot:latest \
    certonly -n \
    --standalone \
    --agree-tos \
    -m ${email} \
    -d ${domain}
}

function update_cert() {
  domain=$1
  new_cert ${domain}
  /bin/cp ./archive/${domain}/fullchain1.pem /srv/resium/resium-nginx/certs/${domain}.cert
  /bin/cp ./archive/${domain}/privkey1.pem /srv/resium/resium-nginx/certs/${domain}.key
}

function main() {
  type=$1
  if [[ "${type}" = "all" || "${type}" = "api" ]]; then
    update_cert ${api_domain}
  fi

  if [[ "${type}" = "all" || "${type}" = "fs" ]]; then
    update_cert ${fs_domain}
  fi

  if [[ "${type}" = "all" || "${type}" = "web" ]]; then
    update_cert ${web_domain}
  fi
}

main "$@"
