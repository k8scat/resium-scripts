# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/1/29

"""
import requests
import config


if __name__ == '__main__':
    payload = {
        'token': config.INTERNAL_TOKEN
    }
    requests.post(f'{config.RESIUM_API}/check_csdn_cookies/', data=payload)
