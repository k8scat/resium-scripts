# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/2/3

"""
import requests
import config


if __name__ == '__main__':
    payload = {
        'token': config.INTERNAL_TOKEN
    }
    requests.post(f'{config.RESIUM_API}/check_qiantu_cookies/', data=payload)
