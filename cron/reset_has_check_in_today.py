# -*- coding: utf-8 -*-

"""
@author: hsowan <hsowan.me@gmail.com>
@date: 2020/2/3

每天定时凌晨4点检查百度文库cookies是否有效

"""
import requests
import config


if __name__ == '__main__':
    payload = {
        'token': config.INTERNAL_TOKEN
    }
    requests.post(f'{config.RESIUM_API}/reset_has_check_in_today/', data=payload)
